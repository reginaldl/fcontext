#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include "fcontext.h"

void func1(void *arg)
{
	if (arg != (void *) 42) {
		printf("ERROR\n");
		exit(-1);
	} else {
		printf("test1\n");
	}
}

int main(int argc, char **argv)
{
	t_fcontext *ctx1;
	t_fcontext current;
	size_t ssize = 16 * 1024;

	ctx1 = malloc(sizeof(*ctx1));
	ctx1->stack.sp = malloc(ssize);
	ctx1->stack.size = ssize;
	ctx1->link = &current;
	fcontext(ctx1, func1, (void *) 42);
	fcontext_swap(&current, ctx1);
	printf("Back to main\n");
	free(ctx1->stack.sp);
	free(ctx1);
	return 0;
}

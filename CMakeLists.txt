cmake_minimum_required(VERSION 2.8)
project(fcontext)

enable_language(ASM)
set(CMAKE_ASM_CREATE_SHARED_LIBRARY ${CMAKE_C_CREATE_SHARED_LIBRARY})

## Architecture ##
execute_process(COMMAND uname -m OUTPUT_VARIABLE ARCHITECTURE)
string(STRIP ${ARCHITECTURE} ARCHITECTURE)
message("Architecture: ${ARCHITECTURE}")
## !Architecture ##

include_directories(.)

## Library ##
add_library(${CMAKE_PROJECT_NAME} STATIC "./${ARCHITECTURE}/fcontext.S")
## !Library ##

## Test ##
enable_testing()
file(GLOB_RECURSE test_src_files "./test/*.c")
foreach (test_src ${test_src_files})
	string(REGEX REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/(.*/)([^/]*).c$" "\\1utest_\\2" bin_var "${test_src}")
	string(REGEX REPLACE ".*/" "" test_name "${bin_var}")
	message("Test found: ${bin_var}")
	add_executable(${test_name} ${test_src})
	set_target_properties(${test_name} PROPERTIES OUTPUT_NAME "${bin_var}")
	target_link_libraries("${test_name}" ${CMAKE_PROJECT_NAME})
	add_test("${test_name}" "${bin_var}")
endforeach (test_src)
## !Test ##

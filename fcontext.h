/**
 * @file   fcontext.h
 * @author Reginald LIPS <reginald.l@gmail.com> - Copyright 2013
 * @date   Wed Oct 10 15:18:43 2012
 *
 * @brief  Header file for fast context.
 *
 */

/*
 * Copyright (c) 2012 Reginald Lips
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions: The above copyright notice and this
 * permission notice shall be included in all copies or substantial
 * portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef FCONTEXT_H_
#define FCONTEXT_H_

#ifdef __i386__
enum {
	FREG_ECX = 0,
	FREG_EDX, // 0x04
	FREG_EDI, // 0x08
	FREG_ESI, // 0x0c
	FREG_EBP, // 0x10
	FREG_EBX, // 0x14
	FREG_ESP, // 0x18
	FREG_EIP, // 0x1c
	NB_FREG
};
typedef int t_register;
#elif __x86_64__
enum {
	FREG_R12 = 0,
	FREG_R13, // 0x08
	FREG_R14, // 0x10
	FREG_R15, // 0x18
	FREG_RDI, // 0x20
	FREG_RBP, // 0x28
	FREG_RBX, // 0x30
	FREG_RSP, // 0x38
	FREG_RIP, // 0x40
	NB_FREG
};
typedef long int t_register;
#elif __arm__
enum {
	FREG_R0 = 0,
	FREG_R1, // 0x04
	FREG_R2, // 0x08
	FREG_R3, // 0x0c
	FREG_R4, // 0x10
	FREG_R5, // 0x14
	FREG_R6, // 0x18
	FREG_R7, // 0x1c
	FREG_R8, // 0x20
	FREG_R9, // 0x24
	FREG_R10, // 0x28
	FREG_R11, // 0x2c
	FREG_R12, // 0x30
	FREG_R13, // 0x34
	FREG_R14, // 0x38
	FREG_R15, // 0x3c
	NB_FREG
};
typedef int t_register;
#else
#error "This architecture is not supported"
#endif

typedef struct s_fstack {
	void *sp;
	size_t size;
} t_fstack;

typedef struct s_fcontext {
	t_register reg[NB_FREG];
	struct s_fcontext *link;
	t_fstack stack;
} t_fcontext;

void fcontext(t_fcontext *ctx, void (*func)(void *ptr), void *arg);
void fcontext_jump(void);
int fcontext_swap(t_fcontext *octx, t_fcontext *nctx);

#endif /* !FCONTEXT_H_ */

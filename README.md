# fcontext
fcontext is a tiny API to impletement fast user contexts (also called coroutine).
It can be considered as a replacement to ucontext. It does not set mmx registers or
sigmask as ucontext does. It improves performances (up to 30%) but float computation
and sighandlers have to be used carefuly accross contexts.
Currently only x84_64 architecture is supported.


## Compilation
The provided CMake file will generate a static library, that can be then linked to your application.

```
$ cmake .
$ make
$ make test
```

## Todo
* Support for ARM

## Copyright
Copyright 2016 Reginad Lips. See LICENSE for details.
